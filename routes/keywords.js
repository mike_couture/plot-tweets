var express = require('express');
var router = express.Router();

var WordCloudData = require('../app/models/word_cloud_data');

/* GET all keywoards for the given timeline */
router.get('/:timeline', function(req, res, next) {
	WordCloudData.find({'timeline_id': req.params.timeline}, function(err, wordCloudData){
        if(err){
            console.log('Error: ', err);
        	res.json({msg: 'Error gathering word cloud data.'});
        }else {
            console.log('Word cloud data returned', wordCloudData.length);
        	res.json(wordCloudData.sort(function(a,b){
                if (a.count < b.count) {
                    return 1;
                }
                if (a.count > b.count) {
                    return -1;
                }
                return 0;
        	}));
        }
    });
});

module.exports = router;
