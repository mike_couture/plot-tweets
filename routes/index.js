/**
*	The index module is responsible solely for rendering templates.
* 	No DB connections should be made in this module. Instead, use the timeline module.
*/
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { activePage: 'index' });
});

/* GET submit timeline page. */
router.get('/submit-timeline', function(req, res, next) {
	res.render('submit-timeline', {
		activePage: 'submit-timeline'
	});
});

/* GET archive page. */
router.get('/archive', function(req, res, next) {
	res.render('archive', {
		activePage: 'archive',
		ngController: 'ArchiveCtrl'
	});
});

/* GET timeline view page */
router.get('/view', function(req, res, next) {
	res.render('view-timeline', {
  		activePage: 'view'
  	});
});

/* GET about page */
router.get('/about', function(req, res, next) {
	res.render('about', {
  		activePage: 'about'
  	});
});

/* GET plot points page. */
router.get('/plotpoints', function(req, res, next) {
	res.render('plotpoints', {
		activePage: 'plotpoints',
		ngController: 'PlotPointsCtrl'
	});
});

module.exports = router;
