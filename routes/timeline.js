/**
*   Contains the requests for timeline objects.
*   GET- Without a param, gets all timelines. With a param, get just that timeline.
*   POST - Add a new timeline.
*   PUT - Updates a timeline.
*/
var express = require('express');
var router = express.Router();

var Timeline = require('../app/models/timeline');

var mongoose = require('mongoose');

/* API calls */

/* GET all timelines */
router.get('/', function(req, res, next) {
    Timeline.find({}, function(err, timelines){
        if(err){
            console.log('Error: ', err);
            res.status(500);
            res.json({ msg: 'Error while gathering timelines.' });
        }else {
            console.log('Timelines returned', timelines.length);
            res.status(200);
            res.json(timelines.sort(function(a,b){
                var da = new Date(a.start).getTime();
                var db = new Date(b.start).getTime();
                if (da > db) {
                    return 1;
                }
                if (da < db) {
                    return -1;
                }
                return 0;
            }));
        }
    });
});

/* GET single timeline */
router.get('/:timelineID', function(req, res, next) {
    Timeline.find({'_id': req.params.timelineID}, function(err, timelines){
        if(err){
            console.log('Error: ', err);
        }else {
            console.log('Timelines returned', timelines.length);
        }
        res.json(timelines);
    });
});

/* POST timeline */
router.post('/', function(req, res) {
    var newTimeline = Timeline.createFromObject(req.body);
    if(newTimeline == null){
        res.send({ msg: 'Invalid date' }); 
    } else{
        newTimeline.save(function(err) {
            if(err){
                console.log('Error', err)
            }else {
                console.log('New timeline created!');
            }
            res.send((err === null) ? { msg: '' } : { msg: err });
        });
    }
});

/* UPDATE (PUT) timeline */
router.put('/:timelineID', function(req, res, next) {
    var to_update = {};
    var body = req.body;
    if(body._id) {
        to_update = req.body;
    } else {
        // The data comes through the PUT as {'object as string' : ''}
        for(var key in req.body) {
            to_update = JSON.parse(key);
        }
    }
    if(!Timeline.validateDates(new Date(to_update.start), new Date(to_update.end))){
        console.log('Invalid date');
        res.send({ msg: 'Invalid date' });
    } else {
        Timeline.update({'_id': req.params.timelineID}, to_update, function(err, raw){
            if(err){
                console.log('Error: ', err);
            }else {
                console.log('Timelines updated:', raw);
                Timeline.find({'_id': req.params.timelineID}, function(err, result){
                    res.send(result[0]);
                });
            }
        });
    }
});

/* Locks all the plot points on the timeline so the public cannot delete or edit them. */
router.get('/lockplotpoints/:timeline', function(req, res, next) {
    Timeline.find({'_id': req.params.timeline}, function(err, timelines){
        if(err){
            console.log('Error: ', err);
        }else {
            console.log('Timelines returned', timelines.length);
            // Should only be 1.
            var timeline = timelines[0];
            timeline.plotpoints.forEach(function(plotpoint){
                plotpoint.is_public = false;
            });
            Timeline.update({'_id': req.params.timeline}, timeline, function(err, raw){
                if(err){
                    console.log('Error: ', err);
                }else {
                    console.log('Timelines updated:', raw);
                }
            });
        }
    });
    res.json('Check logs');
});

module.exports = router;