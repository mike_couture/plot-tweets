var express = require('express');
var path = require('path');

var Config = require('../config');
var conf = new Config();

var fs = require('fs')
var Log = require('log');
var tweetLogger, listenLogger, processLogger;

if(conf.log_dir){
	tweetLogger = new Log('info', fs.createWriteStream(path.join(conf.log_dir, 'crons', 'tweets.log')));
	listenLogger = new Log('info', fs.createWriteStream(path.join(conf.log_dir, 'crons', 'listen.log')));
	processLogger = new Log('info', fs.createWriteStream(path.join(conf.log_dir, 'crons', 'process.log')));
} else {
	tweetLogger = new Log('info');
	listenLogger = new Log('info');
	processLogger = new Log('info');
}

var moment = require('moment');
// Twitter
var twitter = require('twitter');
var keyword_extractor = require("keyword-extractor");
var router = express.Router();

// Make a request to hook into the twitter stream.
var client = new twitter({
	consumer_key: conf.twitter_consumer_key,
	consumer_secret: conf.twitter_consumer_secret,
	access_token_key: conf.twitter_access_token_key,
	access_token_secret: conf.twitter_access_token_secret
});

var Timeline = require('../app/models/timeline');
var Tweet = require('../app/models/tweet');
var ChartData = require('../app/models/chart_data');
var WordCloudData = require('../app/models/word_cloud_data');
var Coordinate = require('../app/models/coordinate');

var mongoose = require('mongoose');

var hashtags = [];
// Keep track of the stream from the callback to destroy it.
var t_stream = null;
var specialChars = ['@', '#', '$', '%', '&'];
var rawTweetDateFormat = 'ddd MMM DD HH:mm:ss ZZ YYYY';
var collectionDateIDFormat = 'YYYY_MM_DD';

function connectToStream(){
	tweetLogger.info('Listening for hashtags: %s', hashtags);
	client.stream('statuses/filter', {track: hashtags.join(',')}, function(stream) {
		t_stream = stream;
		t_stream.on('data', onTweet);
		t_stream.on('error', onError);
	});
};

function updateStream(){
	// The stream cannot actually be updated, instead we need to quickly kill and re-request the stream
	if(t_stream){
		t_stream.destroy();
		client.stream('statuses/filter', {track: hashtags.join(',')}, function(stream) {
			t_stream = stream;
			t_stream.on('data', onTweet);
			t_stream.on('error', onError);
			tweetLogger.info('Updated stream: %s', hashtags);
		});
	} else {
		connectToStream();
	}
};

function killStream(){
	if(t_stream) {
		t_stream.destroy();
	}
	t_stream = null;
	tweetLogger.info('Stopped listening. Waiting for timelines...');
};

function onTweet(tweet){
	var tweet = Tweet.createFromObject(tweet);
	if(tweet === null){
		return; // Skip this item
	}
	tweet.save(function(err){
		if(err){
			tweetLogger.error('Error: %s', err);
		} else{
			tweetLogger.info('Added: %s', tweet.text);
		}
	});
};

function onError(error){
	// Continue parsing tweet!
	tweetLogger.error('Error: %s', error);
};

function filterUnwantedWords(word){
	// Exclude words that contain the following criteria:
	//	Hashtag (#)
	//  URL (http)
	//	Retweet (@)(RT)
	return word.indexOf('#') == -1 &
		 word.indexOf('@') == -1 &&
		 word.indexOf('http') == -1 &&
		 word.toLowerCase() != 'rt' &&
		 word != '&amp' &&
		 word != 'de';
};

router.get('/listen', function(req, res, next){
	hashtags = [];
	// Get all the timelines from the DB that are pending processing.
	Timeline.find({ 'status': { $in: [1,2] } }, function(err, timelines){
		if(timelines.length == 0){
			listenLogger.info('No timelines found that are ready. Waiting a minute...');
			res.json('No timelines to run.\n');
		} else{
			// Go through each timeline and see if it needs to be started.
		    var now = new Date();
		    now.setSeconds(0, 0);
		    var running = [], stopping = [];
		    timelines.forEach(function(timeline){
		    	var start = new Date(timeline.start);
		    	var end = new Date(timeline.end);
		    	if(start <= now && end > now) {
		    		running.push(timeline._id);
		    		hashtags = hashtags.concat(timeline.hashtags);
		    	}
		    	// If any are ended, their hastags will not be added to the list.
		    	// Store them in a list to update their DB status.
		    	if(end <= now){
		    		stopping.push(timeline._id);
		    	}
		    });
		    // If there are any to run, begin listening.
		    if(running.length){
		    	updateStream();
    			Timeline.update({ '_id': { $in: running } }, { 'status': 2 }, {multi: true}, function(err, raw){
    				if(err){
    					listenLogger.error('Error: %s', err);
    				} else{
    					listenLogger.info('Set status to listening: %s', running);
    				}
    			});
		    } else{
		    	killStream();
		    }
    		// Set the status of each timeline to 'processing tweets'
			if(stopping.length) {
    			Timeline.update({ '_id': { $in: stopping } }, { 'status': 3 }, {multi: true}, function(err, raw){
    				if(err){
    					listenLogger.error('Error: %s', err);
    				} else{
    					listenLogger.info('Set status to processing tweets: %s', stopping);
    				}
    			});
			}
			res.json('Listening for: ' + hashtags);
		}
	});
});

router.get('/process', function(req, res, next){
	// Get all timelines with a status of 3 (processing tweets).
	Timeline.find({ 'status': 3 }, function(err, timelines){
    	res.json('Processing', timelines.length, 'timelines.\n');
		timelines.forEach(function(timeline){
			processLogger.info('Processing %s %s', timeline._id, timeline.name);
			var hash = {};
		    var allWords = {};
		    var coordinates = [];
			// Get the start and end dates. These will determine which collections to use.
			var start = moment(timeline.start).format(collectionDateIDFormat);
			var end = moment(timeline.end).format(collectionDateIDFormat);
			var collections = [start];
			if(start != end){
				collections.push(end);
			}
			var collectionsParsed = 0;
			for(var i = 0; i < collections.length; i++){
				var collectionName = collections[i];
				var TweetModel = Tweet.getTweetModel(collectionName);

				var q = buildRawTweetQuery(timeline);
				var stream = TweetModel.find(q).stream();
				stream.on('data', function (tweet) {
				 	parseRawTweet(tweet, hash, allWords, coordinates);
				}).on('error', function (err) {
					processLogger.error('Error: %s', err);
				}).on('close', function () {
					collectionsParsed++;
					if(collectionsParsed == collections.length){
						// Process the results
						processLogger.info('All tweets have been processed for %s', timeline._id);
						parseChartData(timeline._id, hash, function(){
							parseWordCloudData(timeline._id, allWords, function(){
								parseCoordinateData(timeline._id, coordinates, function(){
									Timeline.update({ '_id': timeline._id }, { 'status': 4 }, {multi: true}, function(err, raw){
						            	if(err){
											processLogger.error('Error: %s', err);
						            	} else{
											processLogger.info('Finished parsing %s', timeline._id);
						            	}
						            });
								})
							});
						});
					}
				});
			}
        });
	});
});
/*
q 			{
outerAnd		$and: [
sdoq				{
sdiaq					$and: [
sdq							{
sdq.created_at					created_at: {
					 					$gte: new Date("2016-04-24T00:00:00.000Z")
/sdq.created_at						}
/sdq						} ,
edq							{
edq.created_at					created_at: {
										$lt: new Date("2016-04-25T00:00:00.000Z")
/edq.created_at					}
/edq						} 
/sdiaq					]
/sdoq				} ,
htoq					{
htq						hashtags: {
								$in: [ 'gameofthrones' ]
/htq					}
/htoq				}
/outerand		]
/q			}
*/

function buildRawTweetQuery(timeline){
	var q = {$and: []};
	// Add the outer and clause
	var outerAnd = q['$and'];
	// Add the first and clause for the dates
	var sdoq = {$and: []};
	var sdiaq = sdoq['$and'];
	var sdq = {};
	sdq.created_at = {$gte: new Date(timeline.start)};
	var edq = {};
	edq.created_at = {$lt: new Date(timeline.end)};
	sdiaq.push(sdq, edq);
	outerAnd.push(sdoq);
	var hashtags = { $in: timeline.hashtags.map(function(hashtag){
		return hashtag.substring(1);
	})};
	outerAnd.push({hashtags: hashtags});
	return q;
};

function parseRawTweet(tweet, chartDataHash, allWords, coordinates){
	var datetime = new Date(tweet.created_at);
    if(isNaN( datetime.getTime())){
        return;
    }
    datetime.setSeconds(0, 0);
    if(chartDataHash[datetime] == null){
        chartDataHash[datetime] = {count: 0, words: {}};
    }
    var obj = chartDataHash[datetime];
    obj.count++;
    var cleanText = tweet.text.replace(/([\[\]])/g, '');
    // Parse the text and insert counts into the words hash
    var keywords = keyword_extractor.extract(cleanText,{
        	language:"english",
        	remove_digits: true,
        	return_changed_case: true,
        	remove_duplicates: false
   		}
   	).filter(filterUnwantedWords);

   	// Increment each word count
    keywords.forEach(function(word){
    	if(obj.words[word] == null){
            obj.words[word] = 0;
        }
    	obj.words[word]++;
    	// Upadate the entry in all words
    	if(allWords[word] == null){
            allWords[word] = 0;
        }
    	allWords[word]++;
    });

    if(tweet.coordinates.length){
    	coordinates.push(tweet.coordinates);
    }
}

// Parses and inserts the chart data.
function parseChartData(timelineID, chartDataHash, callback){
	var toInsert = [];
    for(var key in chartDataHash) {
    	var hashObj = chartDataHash[key];
    	// Sort the words and only include the top 100.
    	var words = Object.keys(hashObj.words).map(function (word) {
            return {word: word, count: hashObj.words[word]};
        }).sort(function(a, b){
        	if (a.count < b.count) {
				return 1;
			}
			if (a.count > b.count) {
			    return -1;
			}
			return 0;
        }).slice(0, 100);
        // Create the object now, and save it to the db.
        toInsert.push({
        	timestamp: new Date(key),
        	count: chartDataHash[key].count,
        	words: words,
        	timeline_id: timelineID
        });
    }
    // Make sure we are not inserting duplicates
    ChartData.remove({ 'timeline_id': timelineID}, function(err){
    	if(err){
    		processLogger.error('Error removing chart data: %s', err);
    	} else{
    		processLogger.info('Removed previous chart data for %s', timelineID);
    	}
	    // Now insert into the chart data collection
	    ChartData.insertMany(toInsert, function(err, docs){
	    	if(err){
	    		processLogger.error('Error inserting chart data: %s', err);
	    	} else{
	    		processLogger.info('Inserted %s chart docs for %s', docs.length, timelineID);
	    	}
			callback();
	    });
    });
}

// Parses and inserts the word cloud data.
function parseWordCloudData(timelineID, allWords, callback){
	var allWordCounts = Object.keys(allWords).map(function (word) {
        return {word: word,
        		count: allWords[word],
   				timeline_id: timelineID};
    }).sort(function(a, b){
    	if (a.count < b.count) {
			return 1;
		}
		if (a.count > b.count) {
			return -1;
		}
		return 0;
    }).slice(0, 150);
    // Set a ratio on each word object to determine the font size in the application
    var highestCount = allWordCounts[0].count;
    allWordCounts.forEach(function(word){
    	word.ratio = word.count / highestCount;
   	});
    // Make sure we are not inserting duplicates
    WordCloudData.remove({ 'timeline_id': timelineID}, function(err){
    	if(err){
    		processLogger.error('Error removing word cloud: %s', err);
    	} else{
    		processLogger.info('Removed previous word cloud data for %s', timelineID);
    	}
	    // Now insert into the word chart data collection
	    WordCloudData.insertMany(allWordCounts, function(err, docs){
	    	if(err){
	    		processLogger.error('Error inserting word cloud: %s', err);
	    	}else {
	    		processLogger.info('Inserted %s word cloud docs for %s', docs.length, timelineID);
	    	}
	    	callback();
	    });
    });
}

function parseCoordinateData(timelineID, coordinates, callback){
	var toInsert = [];
	coordinates.forEach(function(c){
		toInsert.push({
			timeline_id: timelineID,
			coordinates: c
		});
	});
	// Make sure we are not inserting duplicates
    Coordinate.remove({ 'timeline_id': timelineID}, function(err){
    	if(err){
    		processLogger.error('Error removing coordinates: %s', err);
    	} else{
    		processLogger.info('Removed coordinates for %s', timelineID);
    	}
	    // Now insert into the coordinate collection
	    Coordinate.insertMany(toInsert, function(err, docs){
	    	if(err){
	    		processLogger.error('Error inserting coordinates: %s', err);
	    	}else {
	    		processLogger.info('Inserted %s coordinate docs for %s', docs.length, timelineID);
	    	}
	    	callback();
	    });
    });
};

// Request to kill twitter stream
router.get('/kill', function(req, res, next) {
	killStream();
	res.json("Stopped listening.");
});

router.get('/convert', function(req, res, next){
	var MongoClient = require('mongodb').MongoClient;
	var ObjectID = require("mongodb").ObjectID;
	var url = 'mongodb://localhost:27017/plot-tweets';
	var async = require('async');

	var _db = null;

 	var parseRawTweet = function(rawObj){
 		//if(rawObj.coordinates.length)
 		//	console.log(rawObj.coordinates);
 		
	    rawObj.start = new Date(rawObj.start);
	    rawObj.end = new Date(rawObj.end);
	    rawObj.display_start = new Date(rawObj.start);
	    rawObj.display_start.setMinutes(rawObj.start.getMinutes() + 30);
	    rawObj.display_end = new Date(rawObj.end);
	    rawObj.display_end.setMinutes(rawObj.end.getMinutes() - 30);

 		Timeline.update({'_id': rawObj._id}, rawObj, function(err, res){
 			console.log(err, res);
 		});
        //var tweet = Tweet.createFromObject(rawObj);
/*
		var tstamp = moment(rawObj.created_at, rawTweetDateFormat).format('YYYY_MM_DD');
		var tmodel = Tweet.getTweetModel(tstamp);

        var tweet = tmodel({
			created_at: new Date(rawObj.created_at),
			id_str: rawObj.id_str,
			text: rawObj.text,
			user: {
				id_str: rawObj.user.id_str,
				name: rawObj.user.name,
				screen_name: rawObj.user.screen_name,
				profile_image_url: rawObj.user.profile_image_url,
				profile_image_url_https: rawObj.user.profile_image_url_https
			},
			hashtags: rawObj.hashtags.map(function(hashtag){return hashtag.toLowerCase();})
	    });
*/
        /*
        var tw = {
			created_at: new Date(rawObj.created_at),
			id_str: rawObj.id_str,
			text: rawObj.text,
			user: {
				id_str: rawObj.user.id_str,
				name: rawObj.user.name,
				screen_name: rawObj.user.screen_name,
				profile_image_url: rawObj.user.profile_image_url,
				profile_image_url_https: rawObj.user.profile_image_url_https
			},
			hashtags: rawObj.entities.hashtags.map(function(hashtag){return hashtag.text.toLowerCase();})
		}
		*/
		/*
		var collection = _db.collection('tweets_2016_04_28');
		// Convert to tweet
		collection.insert(rawObj, function(err, res){
			if(err){
        		console.log('Stahp', err)
        	} else{
        		console.log('Saved');
        	}
		});
*/
/*
		if(tweet){
	        tweet.save(function(err, res){
	        	if(err){
	        		console.log('Stahp', err)
	        	} else{
	        		console.log('Saved');
	        	}
	        });
		}
*/

    };

    var queue = async.queue(function(doc, callback){
        parseRawTweet(doc);
        callback();
    }, Infinity);

	// Get all tweets from the db to convert
	MongoClient.connect(url, function(err, db){
		_db = db;
		console.log('Start');
		var collection = _db.collection('timelines');
		var cursor = collection.find({});
        cursor.forEach(function(doc){
            queue.push(doc, function(err){});
        });
        queue.drain = function() {
            if (cursor.isClosed()) {
                console.log('all have been processed');
                _db.close();
            }
        }
	});
	res.json('convert');
})

module.exports = router;