var express = require('express');
var router = express.Router();

var ChartData = require('../app/models/chart_data');
var Coordinate = require('../app/models/coordinate');

/* GET all parsed tweets for the given timeline */
router.get('/:timeline', function(req, res, next) {
	ChartData.find({'timeline_id': req.params.timeline}, function(err, chartData){
        if(err){
            console.log('Error: ', err);
            res.json({msg: 'Error gathering tweets.'});
        }else {
            console.log('Chart data returned', chartData.length);
            // Sort the data by date
            res.json(chartData.sort(function(a, b){
                var da = new Date(a.timestamp).getTime();
                var db = new Date(b.timestamp).getTime();
                if (da > db) {
                    return 1;
                }
                if (da < db) {
                    return -1;
                }
                return 0;
            }));
        }
    });
});

/* GET all coordinates for the given timeline */
router.get('/coordinates/:timeline', function(req, res, next) {
	Coordinate.find({'timeline_id': req.params.timeline}, function(err, coordinates){
        if(err){
            console.log('Error: ', err);
        }else {
            console.log('Coordinates returned', coordinates.length);
        }
        res.json(coordinates);
    });
});

module.exports = router;
