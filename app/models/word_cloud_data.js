var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var wordCloudDataSchema = new Schema({
	timeline_id: String,
	count: Number,
	word: String,
	ratio: Number
});

var WordCloudData = mongoose.model('WordCloudData', wordCloudDataSchema, 'word_cloud_data');

module.exports = WordCloudData;
