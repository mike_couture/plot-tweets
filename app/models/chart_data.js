var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var chartDataSchema = new Schema({
	timeline_id: String,
	count: Number,
	words: Array,
	timestamp: Date
});

var ChartData = mongoose.model('ChartData', chartDataSchema, 'chart_data');

module.exports = ChartData;
