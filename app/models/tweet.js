var moment = require('moment');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var rawTweetDateFormat = 'ddd MMM DD HH:mm:ss ZZ YYYY';
var tweetModels = {};

var tweetSchema = new Schema({
	created_at: Date,
	id_str: String,
	text: String,
	user: {
		id_str: String,
		name: String,
		screen_name: String,
		profile_image_url: String,
		profile_image_url_https: String
	},
	hashtags: Array,
	coordinates: Array
});

module.exports = {};

module.exports.schema = tweetSchema;

module.exports.tweetModels = tweetModels;

module.exports.createFromObject = function(rawObj){
	if(rawObj == null) {
		return null;
	}
	// If there is no created_at, the message was most likely a rate_limit.
	if(!rawObj.created_at) {
		return null;
	}
	// Only english for now. TODO: Handle the keyword parser for foreign languages
	if(rawObj.user.lang != 'en') {
		return null;
	}
	var tstamp = moment(rawObj.created_at, rawTweetDateFormat).format('YYYY_MM_DD');
	var Tweet = this.getTweetModel(tstamp);
	return Tweet({
		created_at: new Date(rawObj.created_at),
		id_str: rawObj.id_str,
		text: rawObj.text,
		user: {
			id_str: rawObj.user.id_str,
			name: rawObj.user.name,
			screen_name: rawObj.user.screen_name,
			profile_image_url: rawObj.user.profile_image_url,
			profile_image_url_https: rawObj.user.profile_image_url_https
		},
		hashtags: rawObj.entities.hashtags.map(function(hashtag){return hashtag.text.toLowerCase();}),
		coordinates: rawObj.coordinates ? rawObj.coordinates.coordinates : []
    });
};

module.exports.getTweetModel = function(tstamp){
	var partitionID = 'tweets_' + tstamp;
	if(this.tweetModels[partitionID] == null){
		this.tweetModels[partitionID] = mongoose.model('Tweet', tweetSchema, 'tweets_' + tstamp);
	}
	return this.tweetModels[partitionID];
};