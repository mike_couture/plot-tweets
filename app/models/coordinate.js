var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var coordinateSchema = new Schema({
	timeline_id: String,
	coordinates: Array,
});

var Coordinate = mongoose.model('Coordinate', coordinateSchema);

module.exports = Coordinate;
