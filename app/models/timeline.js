var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var timelineSchema = new Schema({
	name: String,
	start: Date,
	end: Date,
	display_start: Date,
	display_end: Date,
	hashtags: Array,
	plotpoints: Array,
	status: Number,
	updated_at: Date,
	created_at: Date
});

// on every save, add the date
timelineSchema.pre('save', function(next) {
	// get the current date
	var currentDate = new Date();

	// change the updated_at field to current date
	this.updated_at = currentDate;

	// if created_at doesn't exist, add to that field
	if (!this.created_at)
	this.created_at = currentDate;

	next();
});

var Timeline = mongoose.model('Timeline', timelineSchema);

module.exports = Timeline;

module.exports.createFromObject = function(rawObj){
    // Adjust the dates here.
    var start = new Date(rawObj.start);
    var end = new Date(rawObj.end);
    if(!Timeline.validateDates(start, end)){
    	return null;
    }
    rawObj.display_start = new Date(rawObj.start);
    rawObj.display_end = new Date(rawObj.end);
    start.setMinutes(start.getMinutes() - 30);
    end.setMinutes(end.getMinutes() + 30);

	return Timeline({
        name: rawObj.name,
        start: start,
        end: end,
        display_start: rawObj.display_start,
        display_end: rawObj.display_end,
        hashtags: rawObj.hashtags,
        plotpoints: rawObj.plotpoints,
        status: rawObj.status
    });
};

module.exports.validateDates = function(start, end){
    if(isNaN(start.getTime()) || isNaN(end.getTime())){
        return false;
    }
    var now = new Date();
    var startMS = start.getTime();
    var endMS = end.getTime();
    if(endMS < now.getTime() || endMS < startMS || startMS == endMS || endMS - startMS > (1000 * 60 * 60 * 6)){
        return false;
    }
    return true;
};