module.exports = function(){
    switch(process.env.NODE_ENV){
        case 'production':
            return require('./confs/prod');
        case 'test':
        	return require('./confs/test');
        case 'development':
        default:
            return require('./confs/dev');
    }
};