$(function () {
	$('#plotpointdatetimepicker').datetimepicker();

	$('#editTimeline').submit(function(event){
		event.preventDefault();
		updateTimeline();
	});

	$('#addPlotPointButton').on('click', function(event){
		event.preventDefault();
		onAddButtonClick();
	});

	var timeline, _id;
	var plotPoints = [];

	$(document).ready(function() {
	    getTimeline();
	});

	function getTimeline(){
		_id = getParameterByName('timelineID');
		$.getJSON( 'edit/' + _id, function( result ) {
			timeline = result[0];
	    	updateForm();
	    });
	};

	function onAddButtonClick(){
		var timestamp = $('#plotpointdatetimepicker').data('DateTimePicker').date().toDate();
		var description = $('#plotPointDescription').val().trim();

		// Clear the description only. The timestamp stays.
		$('#plotPointDescription').val('');

		addPlotPoint({timestamp: timestamp, description: description});
	};

	function addPlotPoint(plotPoint){
		plotPoints.push(plotPoint);
		updatePlotPointTable();
	};

/**

									td
										div#plotpointdatetimepicker.input-group.date
											input.form-control(type='text')
											span.input-group-addon
												span.glyphicon.glyphicon-calendar
									td
										input#plotPointDescription.form-control(type='text')
									td
										button#addPlotPointButton.btn.btn-success.btn-add.center-block(type='button')
											span.glyphicon.glyphicon-plus

*/
	function updatePlotPointTable() {
		d3.select('#plotPointTable tbody').selectAll("tr.plot-point").remove();

		var rows = d3.select('#plotPointTable tbody')
			.selectAll('tr.plot-point')
			.data(plotPoints).enter()
			.append('tr')
			.attr('class', 'plot-point');

		rows.append('td')
			.text(function(d){
				return moment(d.timestamp).format('M/D/YYYY - h:mm A');
			});

		rows.append('td').append('div')
			.attr('class', 'trim')
			.text(function(d){
				return d.description
			});

	    var actionRow = rows.append('td').attr('class', 'clearfix actions');
		    actionRow.append('span')
		    	.attr('class', 'glyphicon glyphicon-pencil pull-left action btn-edit');
		    actionRow.append('span')
		    	.attr('class', 'glyphicon glyphicon-trash pull-right action btn-delete');

		actionRow.selectAll('.btn-delete').on('click', function(d){
			plotPoints.splice(plotPoints.indexOf(d), 1);
			updatePlotPointTable();
	    });

	    actionRow.selectAll('.btn-edit').on('click', function(d){
	    	// Editing is just replacing the form input elements.
	    	$('#plotpointdatetimepicker').data('DateTimePicker').date(d.timestamp);
			$('#plotPointDescription').val(d.description);
	    });
	};

	var updateForm = function(){
		var name = timeline.name;
		$('#timelineName').val(name);
		var start = new Date(timeline.start);
		var end = new Date(timeline.end);
        $('#startdatetimepicker').data('DateTimePicker').date(start);
        $('#enddatetimepicker').data('DateTimePicker').date(end);
		var _hashtags = timeline.hashtags;
		if(_hashtags.constructor !== Array)
			_hashtags = [_hashtags];
		_hashtags.forEach(function(hashtag){
			// Remove the # if it exists.
			addHashtag(hashtag.replace('#', ''));
		});
		// For simplicity, set the plot point picker to the start date
        $('#plotpointdatetimepicker').data('DateTimePicker').date(start);
        plotPoints = timeline.plotpoints || [];
		if(plotPoints.constructor !== Array)
			plotPoints = [_plotPoints];
		plotPoints.forEach(function(plotpoint){
			// Convert to moment date
			plotpoint.timestamp = new Date(plotpoint.timestamp);
		});
		updatePlotPointTable();
	};

	var updateTimeline = function() {
        var start = $('#startdatetimepicker').data('DateTimePicker').date().toDate();
        var end = $('#enddatetimepicker').data('DateTimePicker').date().toDate();
		// TODO: Validation
        var _timeline = new Timeline();
        _timeline.name = $('#timelineName').val();
        _timeline.start = start;
        _timeline.end = end;
        _timeline.hashtags = hashtags;
        _timeline.plotpoints = plotPoints;
        // TODO: Do not make this ready right away
        _timeline.status = timeline.status;

        // Post to the DB
        $.ajax({
            type: 'PUT',
            data: JSON.stringify(_timeline),
            url: '/edit/' + timeline._id,
			contentType: 'application/json',
            dataType: 'JSON'
        }).done(function( response ) {
            // Check for successful (blank) response
            if (response.msg === '') {
                window.location.replace('archive');
            }
            else {
                 console.log('Error: ' + response.msg);
            }
        });
	};
});