var wordCloudLayout, created=false;

function redrawWordCloud(){
	if(created){
		return;
	}
	created = true;
	wordCloudLayout.stop().start();
};

function initializeWordCloud(){
	var fsize = 100;
	var highestCount = 0, lowestCount = Number.MAX_SAFE_INTEGER, largestWord;
	// Find the largest words and get the length of chars.
	// The w and h will be based on that, so it won't spill off the edge.
	keywords.forEach(function(keyword){
		if(keyword.count > highestCount){
			highestCount = keyword.count;
			largestWord = keyword.word;
		}
		if(keyword.count < lowestCount){
			lowestCount = keyword.count;
		}
	});
	var maxFont = Math.min(fsize, fsize * (6 / largestWord.length));

	var fontSize = d3.scale['log']()
		.domain([lowestCount, highestCount])
		.range([10, maxFont]);

	var fill = d3.scale.category20c();
	var w = 900, h = 600;
	var scale = 1;

	wordCloudLayout = d3.layout.cloud()
		.size([w, h])
		.fontSize(function(t) {
	    	return fontSize(t.count);
		})
		.text(function(t) {
	    	return t.text
		})
	    .padding(1)
		.font('Helvetica')
		.spiral('archimedean')//rectangular 
		.rotate(function() { return 0; })
		.words(keywords.map(function(d) {
      		return {text: d.word, count: d.count, opacity: Math.max(0.3, 1 - (d.ratio * 1))};
    	}))
		.on("end", draw);

	function draw(t, e){
		d3.select('#wordCloud').selectAll('*').remove();

		var svg = d3.select('#wordCloud')
			.append('svg')
		      	.attr('width', '100%')
		      	.attr('height', '100%')
				.attr('preserveAspectRatio', 'xMinYMin meet')
				.attr('viewBox', '0 0 300 650')
				.classed('svg-content-responsive', true);

		var scale = e ? Math.min(w / Math.abs(e[1].x - w / 2), w / Math.abs(e[0].x - w / 2), h / Math.abs(e[1].y - h / 2), h / Math.abs(e[0].y - h / 2)) / 2 : 1;

	    // Header
	    svg.append('text')
	        .attr('class', 'viz-header')
	        .attr('y', 20)
	        .attr('x', w)
	        .attr('font-size', 24)
	        .attr('text-anchor', 'middle')
	        .text(timeline.name);

		// Word Cloud
	    var wordArea = svg.append('g')
	        .attr('transform', 'translate(' + [0, 10] + ')');

	    wordArea.append("g")
			.attr("transform", "translate(" + [w, (h >> 1) + 40] + ")scale(" + scale + ")").selectAll("text")
	    .data(t, function(t) {
	        	return t.text;
	    	}).enter()
	    	.append("text")
		    	.attr("text-anchor", "middle")
		    	.attr("transform", function(t) {
		        	return "translate(" + [t.x, t.y] + ")";
		    	})
		    	.style("font-size", function(t) {
		        	return t.size + "px";
		    	})
		    	.style("font-family", function(t) {
		        	return t.font
		    	})
		    	.style("fill", function(t) {
		       		return fill(t.text.toLowerCase())
		    	})
		    	.text(function(t) {
		        	return t.text
		    	});
	}
};