$(function () {
    $('#plotpointdatetimepicker').datetimepicker();

    $('#addPlotPointButton').on('click', function(event){
        event.preventDefault();
        onAddButtonClick();
    });

    $('#savePlotPointsBtn').on('click', function(event){
        event.preventDefault();
        savePlotPoints();
    });
});

function onAddButtonClick(){
    var timestamp = $('#plotpointdatetimepicker').data('DateTimePicker').date().toDate();
    var description = $('#plotPointDescription').val().trim();

    // Clear the description only. The timestamp stays.
    $('#plotPointDescription').val('');

    addPlotPoint({timestamp: timestamp, description: description, is_public: true});
};

function addPlotPoint(plotPoint){
    plotPoints.push(plotPoint);
    updatePlotPointTable();
};

function updatePlotPointEntries(){
    // For simplicity, set the plot point picker to the start date
    var start = new Date(timeline.start);
    $('#plotpointdatetimepicker').data('DateTimePicker').date(start);
    updatePlotPointTable();
};

function updatePlotPointTable() {
    d3.select('#plotPointTable tbody').selectAll('tr.plot-point').remove();

    var rows = d3.select('#plotPointTable tbody')
        .selectAll('tr.plot-point')
        .data(plotPoints).enter()
        .append('tr')
        .attr('class', 'plot-point');

    rows.append('td')
        .text(function(d){
            return moment(d.timestamp).format('M/D/YYYY - h:mm A');
        });

    rows.append('td').append('div')
        .attr('class', 'trim')
        .text(function(d){
            return d.description
        });

    var actionRow = rows.append('td').attr('class', 'clearfix actions');
    actionRow.append('span')
        .attr('class', function(d){
            return d.is_public ? 'glyphicon glyphicon-trash pull-right action btn-delete' : null;
        });

    actionRow.selectAll('.btn-delete').on('click', function(d){
        plotPoints.splice(plotPoints.indexOf(d), 1);
        updatePlotPointTable();
    });
};

function savePlotPoints() {
    // TODO: Validation
    // Post to the DB
    $.ajax({
        type: 'PUT',
        data: JSON.stringify({ plotpoints: plotPoints }),
        url: '/timeline/' + timeline._id,
        dataType: 'json',
        success: function(response) {
            timeline = new Timeline(response);
            plotPoints = timeline.plotpoints.concat();
            $('#plotPointModal').modal('hide');
            drawChart();
        },
        error: function(jqXHR,  error){
            console.log(error);
        }
    })
};

function drawChart(){
    var width = parseInt(d3.select('#chart').style('width')) - margin.left - margin.right;
    var height = 600 - margin.top - margin.bottom;

    width = 1920 - margin.left - margin.right;
    height = 512;

    var ppTimeLineHeight = 20;

    // Create date range from the start of the timeline to the end.
    // Can't base it on the data, some timestamps may be missing.
    var start = new Date(timeline.start);
    var end = new Date(timeline.end);
	var timestamps = [];
	for (var d = start; d < end; d.setMinutes(d.getMinutes() + 1)) {
	    timestamps.push(new Date(d));
	}

    xScale = d3.scale.ordinal()
	    .domain(timestamps)
        .rangeRoundBands([0, width], 0.1);

    yScale = d3.scale.linear()
    	.domain([0, d3.max(data.map(function(d) {
	        return d.count;
	    }))])
        .rangeRound([height, 0]);

    var aAxisTickValues = data.map(function(d, i){
	    	return (d.date.getMinutes() % 15 == 0) ? d.date : null;
		}).filter(function(d){
			return !!d;
		});

	// Using rangeRoundBbands gives us a clean distance between each column,
	// but throws the leftover floats to each side of the chart.
	// Sooooo, grab the offset and shift EVERYTHING left twice the offset! Math!
	var leftOuter = xScale.range()[0];
	width = width - leftOuter - leftOuter;

    var svg = d3.select('#chart').selectAll('*').remove();

    svg = d3.select('#chart').append('div')
		.classed('svg-container', true) //container class to make it responsive
	.append('svg')
	   //responsive SVG needs these 2 attributes and no width and height attr
	   .attr('preserveAspectRatio', 'xMinYMin meet')
	   .attr('viewBox', '0 0 1900 1200')
	   //class to make it responsive
	   .classed('svg-content-responsive', true)
    .append('g')
        .attr('transform', 'translate(' + (margin.left + leftOuter) + ',' + margin.top + ')');

    svg.append('defs').append('linearGradient')
        .attr('id', 'line-gradient')
        .attr('x1', '0%').attr('y1', '0%')
        .attr('x2', '0%').attr('y2', '100%')
    .selectAll('stop')
        .data([
            {offset: '0%', color: 'white', opacity: 1},
            {offset: '45%', color: '#555555', opacity: 1},
            {offset: '100%', color: '#555555', opacity: 1}
        ])
    .enter().append('stop')
        .attr('offset', function(d) { return d.offset; })
        .attr('stop-color', function(d) { return d.color; })
        .attr('stop-opacity', function(d) { return d.opacity; });

    var xAxis = d3.svg.axis()
        .scale(xScale)
        .orient('bottom')
        .tickSize(0)
        .tickFormat(function(d) {
            return pDateFormat(d).toLowerCase(); 
        })
        .tickValues(aAxisTickValues);

    var yAxis = d3.svg.axis()
        .scale(yScale)
        .orient('left')
        .outerTickSize(0);

    // Header
    svg.append('text')
        .attr('class', 'viz-header')
        .attr('font-size', 24)
        .text(timeline.name);

    // Chart
    var chartArea = svg.append('g')
        .attr('transform', 'translate(' + [0, 10] + ')');

    // Bg for the pp axis
    var ppbg = chartArea.append('rect')
        .attr('class', 'axis-fill')
        .attr('width', width)
        .attr('height', ppTimeLineHeight)
        .attr('transform', 'translate(0,' + height + ')');
    ppbg.on('dblclick', function(d){
        console.log(d);
    });

    // Add plot points button
    var addPPBtn = chartArea.append('g')
        .attr('transform', 'translate(' + [-20, height] +')');

    addPPBtn.append('rect')
        .attr('class', 'add-pp-btn-bg')
        .attr('width', 20)
        .attr('height', ppTimeLineHeight);
    addPPBtn.append('line')
        .attr('class', 'plus-icon')
        .attr('x1', 10)
        .attr('y1', 5)
        .attr('x2', 10)
        .attr('y2', 15);
    addPPBtn.append('line')
        .attr('class', 'plus-icon')
        .attr('x1', 5)
        .attr('y1', 10)
        .attr('x2', 15)
        .attr('y2', 10);

    // Hit area for button
    addPPBtn.append('rect')
        .attr('class', 'cursor-pointer hit-area')
        .attr('width', 20)
        .attr('height', ppTimeLineHeight)
        .on('mouseover', function(d){
            var nodeBounds = d3.select(this).node().getBoundingClientRect();
            addbtntip.style({
                'left': (nodeBounds.left - 35) + 'px',
                'top': (nodeBounds.top - 30) + 'px',
                'opacity': 1
            });
        })
        .on('mouseout', function(d){
            addbtntip.style({
                'left': '0px',
                'top': '0px',
                'opacity': 0
            });
        })
        .on('click', function(d){
            $('#plotPointModal').modal({
                backdrop: 'static'
            });
            updatePlotPointEntries();
        });

	 // Add plot points below the chart features
	var pps = chartArea.selectAll('g.plot-point')
	 	.data(plotPoints).enter()
	 	.append('g')
	 	.attr('class', 'plot-point')
	 	.attr('transform', function(d){
	 		return 'translate(' + (xScale(d.timestamp) - leftOuter) + ',' + (height + 1) + ')';
	 	});

	pps.append('rect')
	 	.attr('class', 'plot-point-icon')
	 	.attr('height', ppTimeLineHeight - 1)
	 	.attr('width', xScale.rangeBand());

    pps.append('rect')
        .attr('class', 'hit-area')
        .attr('width', 50)
        .attr('height', ppTimeLineHeight);

    pps.on('mouseover', function(d){
    	pptooltip.html(function() {
            return pDateFormat(d.timestamp).toLowerCase() + ': ' + d.description;
        });
        var ttW = tooltip.node().getBoundingClientRect().width;
        var bodyBounds = document.body.getBoundingClientRect();
        var nodeBounds = d3.select(this).node().getBoundingClientRect();

        var chartBound = d3.select('#chart').node().getBoundingClientRect();
        var xPos = nodeBounds.left - bodyBounds.left;
        if(xPos + ttW >= chartBound.right)
            xPos -= ttW;
        var yPos = nodeBounds.top - bodyBounds.top;
        pptooltip.style({
            'left': xPos + 'px',
            'top': (yPos + 20) + 'px',
            'opacity': 1
        });

    	d3.select(this).selectAll('rect')
    		.classed('over', true);
    	// Get the next 5 columns and apply a gradient of colors to them.
    	var relatedTweets = [];
    	tweets.each(function(t, i){
    		if(t.date.getTime() >= d.timestamp.getTime() && t.date.getTime() < (d.timestamp.getTime() + (millisecondsInMinute * 5))){
    			relatedTweets.push(t);
    		}
    	});
    	tweets.selectAll('.highlight-column')
    		.attr('class', function(t){
    			if(relatedTweets.indexOf(t) > -1){
    				return d3.select(this).attr('class') + ' o' + relatedTweets.indexOf(t);
    			}
    			return d3.select(this).attr('class');
    		});
    });

    pps.on('mouseout', function(d){
        pptooltip.style({
            'left': '0px',
            'top': '0px',
            'opacity': 0
        });
    	d3.select(this).selectAll('rect')
    		.classed('over', false);
    	tweets.selectAll('.highlight-column')
    		.classed('o0', false)
    		.classed('o1', false)
    		.classed('o2', false)
    		.classed('o3', false)
    		.classed('o4', false);
    });

    // Chart border
    // Bottom
    chartArea.append('g')
        .attr('class', 'chart-border')
    .append('line')
        .attr('x1', 0)
        .attr('y1', yScale(0))
        .attr('x2', width)
        .attr('y2', yScale(0));
    // Right
    chartArea.append('g')
        .attr('class', 'chart-border')
    .append('line')
        .attr('x1', width)
        .attr('y1', 0)
        .attr('x2', width)
        .attr('y2', height + 1);
    // Top
    chartArea.append('g')
        .attr('class', 'chart-border')
    .append('line')
        .attr('x1', 0)
        .attr('y1', 0)
        .attr('x2', width)
        .attr('y2', 0);

	// Draw axes
    chartArea.append('g')
        .attr('class', 'x axis')
        .attr('transform', 'translate(' + (-leftOuter) + ',' + (height + ppTimeLineHeight + 5) + ')')
        .call(xAxis);

    chartArea.append('g')
        .attr('class', 'y axis')
        .call(yAxis);

    // Keywords
    chartArea.append('g')
        .attr('class', 'keywords')
        .attr('transform', 'translate(' + [0, height + ppTimeLineHeight + 25] + ')');

    d3.select('g.keywords').selectAll('*').remove();
    d3.select('g.keywords')
        .append('foreignObject')
            .attr('class', 'keyword-text')
            .attr('width', '100%')
            .attr('height', 40)
        .append('xhtml:body')
        .append('xhtml:div')
            .attr('class', 'keywords-container')
            .html(function(){
                var text = 'Keywords (total):<br/>';
                for(var i = 0; i < Math.min(5, keywords.length); i++){
                    text+= (i + 1) + ': <span class="keyword">' + keywords[i].word + '</span><span class="count"> (' + keywords[i].count + ')</span>';
                }
                return text;
            });

	// Draw columns
    var tweets = chartArea.selectAll('.tweets')
        .data(data)
    .enter().append('g')
        .attr('class', 'g tweets')
        .attr('transform', function(d) { return 'translate(' + (xScale(d.date) - leftOuter) + ',0)'; });

    // Adds an invisible column to act as the hit area
    tweets.append('rect')
    	.attr('class', 'hit-area')
        .attr('width', xScale.rangeBand())
        .attr('y', 0)
        .attr('height', height);

    tweets.append('rect')
    	.attr('class', 'column').call(drawColumn);

    // Adds an invisible column to act as the highlight overlay
    tweets.append('rect')
    	.attr('class', 'highlight-column').call(drawColumn);

    function drawColumn(g){
        g.attr('width', xScale.rangeBand())
        .attr('y', function(d) {
            return yScale(+d.count);
        })
        .attr('height', function(d) {
            return yScale(0) - yScale(+d.count);
        });
    };

    tweets.on('mouseover', function(d){
     	tooltip.html(function() {
            var tt = 'Range: <strong>' + tooltipStartFormat(d.date) + ' - ' + tooltipEndFormat(d.date) + '</strong><br/>';
            tt += 'Total Tweets: <strong>' + numeral(+d.count).format('0,0') + '</strong>';
            return tt;
        });

        d3.select('g.keywords').selectAll('*').remove();
        d3.select('g.keywords')
            .append('foreignObject')
                .attr('class', 'keyword-text')
                .attr('width', '100%')
                .attr('height', 40)
            .append('xhtml:body')
            .append('xhtml:div')
                .attr('class', 'keywords-container')
                .html(function(){
                    var text = 'Keywords:<br/>';
                    for(var i = 0; i < Math.min(5, d.words.length); i++){
                        text+= (i + 1) + ': <span class="keyword">' + d.words[i].word + '</span><span class="count"> (' + d.words[i].count + ')</span>';
                    }
                    return text;
                });

        d3.select(this).select('rect.column')
            .classed('over', true);
        var ttW = tooltip.node().getBoundingClientRect().width;
        var ttH = tooltip.node().getBoundingClientRect().height;
        var chartBound = d3.select('#chart').node().getBoundingClientRect();
        var xPos = d3.event.pageX - (ttW / 2);
        if(xPos + ttW >= chartBound.right)
            xPos -= (ttW / 2);
        var yPos = d3.event.pageY - (ttH + 60);
        if(yPos < chartBound.top)
        	yPos += (ttH + 60 + 15);

        tooltip.style({
            'left': xPos + 'px',
            'top':  + yPos + 'px',
            'opacity': 1
        });
    });

    tweets.on('mouseout', function(d, i){
        d3.select(this).select('rect.column')
            .classed('over', false);
        tooltip.style({
            'left': '0px',
            'top': '0px',
            'opacity': 0
        });
        d3.select('g.keywords').selectAll('*').remove();
        d3.select('g.keywords')
            .append('foreignObject')
                .attr('class', 'keyword-text')
                .attr('width', '100%')
                .attr('height', 40)
            .append('xhtml:body')
            .append('xhtml:div')
                .attr('class', 'keywords-container')
                .html(function(){
                    var text = 'Keywords (total):<br/>';
                    for(var i = 0; i < Math.min(5, keywords.length); i++){
                        text+= (i + 1) + ': <span class="keyword">' + keywords[i].word + '</span><span class="count"> (' + keywords[i].count + ')</span>';
                    }
                    return text;
                });
    });

    // Move the info box
    d3.selectAll('#plotPointInfo')
    	.style('margin-left', function(){
    		return (margin.left + leftOuter) + 'px';
    	});
};
