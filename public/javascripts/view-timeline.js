var plotPoints = [], keywords = [];

var millisecondsInMinute = 60 * 1000;

var margin = {top: 20, right: 40, bottom: 50, left: 55};

var pDateFormat = d3.time.format("%_I:%M%p");
var tooltipStartFormat = d3.time.format("%_I:%M:%S");
var tooltipEndFormat = d3.time.format("%_I:%M:59");

var timeline, data, xScale, yScale, tooltip;

var map, heatmap;

$(function () {
	$(document).ready(function() {
	    getTimelineData();
	});

	$('.display-nav ul a[href="#mapContainer"]').on('shown.bs.tab', function(){
        var center = map.getCenter();
	    google.maps.event.trigger(map, 'resize');
	    map.setCenter(center);
	});

	$('.display-nav ul a[href="#wordCloudContainer"]').on('shown.bs.tab', function(){
		redrawWordCloud();
	});

	$('#showChartBtn').change(function(event){
		event.preventDefault();
		d3.select('#wordCloudContainer').classed('hidden', true);
		d3.select('#chartContainer').classed('hidden', false);
		d3.select('#mapContainer').classed('hidden', true);
	});

	$('#showWordCloudBtn').change(function(event){
		event.preventDefault();
		d3.select('#wordCloudContainer').classed('hidden', false);
		d3.select('#chartContainer').classed('hidden', true);
		d3.select('#mapContainer').classed('hidden', true);
	});

	$('#showMapBtn').change(function(event){
		event.preventDefault();
		d3.select('#wordCloudContainer').classed('hidden', true);
		d3.select('#chartContainer').classed('hidden', true);
		d3.select('#mapContainer').classed('hidden', false);
	});

	function getTimelineData(){
		var param = getParameterByName('timelineID');
		$.getJSON( '/timeline/' + param, function( result ) {
			timeline = new Timeline(result[0]);
            plotPoints = timeline.plotpoints.concat();
			$.getJSON( '/keywords/' + param, function( result ) {
				keywords = result;
			    $.getJSON( '/tweets/' + param, function( result ){
			    	data = result;
				    data.forEach(function(d) {
				    	// Convert to an actual date object
				    	d.date = new Date(d.timestamp);
				    });

		            // Create the tooltip div
			        tooltip = d3.select('body #content').append('div')   
			            .attr('id', 'tooltip')
			            .attr('class', 'pt-tooltip');
			        pptooltip = d3.select('body #content').append('div')   
			            .attr('id', 'plot-point-tooltip')
			            .attr('class', 'pt-tooltip');
			        addbtntip = d3.select('body #content').append('div')   
			            .attr('id', 'add-pp-tooltip')
			            .attr('class', 'pt-tooltip')
			            .text('Add Plot Point');
		    		drawChart();
				    initializeWordCloud();
					$('#mapHeader').text(timeline.name);
				    $('.display-nav').addClass('in');
				    $('.display-nav ul a[href="#chartContainer"]').tab('show');
				    $('#loadingContainer').hide();
			    });
		    });
	    });
	};
});

function drawMap(){
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 4,
		minZoom: 2,
		center: new google.maps.LatLng(40.565967, -96.760854),
		mapTypeId: google.maps.MapTypeId.SATELLITE
	});
	var gradient = [
		'rgba(255, 255, 255, 0)',
		'rgba(24, 0, 255, 1)',
		'rgba(14, 116, 255, 1)',
		'rgba(0, 203, 255, 1)',
		'rgba(0, 255, 255, 1)',
		'rgba(127, 255, 0, 1)',
		'rgba(254, 255, 0, 1)',
		'rgba(255, 203, 0, 1)',
		'rgba(255, 153, 0, 1)',
		'rgba(255, 79, 0, 1)',
		'rgba(255, 69, 69, 1)'
	];
	getMapPoints(function(coordinates){
	    heatmap = new google.maps.visualization.HeatmapLayer({
			data: coordinates,
			map: map,
			gradient: gradient,
			radius: 20,
			opacity: 1,
			dissipating: true
	    });
	});
};

function getMapPoints(callback){
	var param = getParameterByName('timelineID'), coordinates = [];
	$.getJSON( '/tweets/coordinates/' + param, function( result ){
		result.forEach(function(coordinate){
			coordinates.push(new google.maps.LatLng(coordinate.coordinates[1], coordinate.coordinates[0]));
		});
		callback(coordinates);
    });
};
