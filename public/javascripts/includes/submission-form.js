var hashtags = [];

// Returns an array containing the two time elements, hours and minutes.
var getTimes = function(elements){
	// Get each value from the elements
	var hours = parseInt(elements[0].value);
	var minutes = parseInt(elements[1].value);
	var ampm = elements[2].value;
	// Hours need to be converted to 24 hours.
	if(ampm == "PM") {
		if(hours < 12) {
			hours += 12;
		}
	} else {
		if(hours == 12) {
			hours = 0;
		}
	}
	return [hours, minutes];
};


// Parses the start adn end dates from a timeline and return an object
var parseDatesForFormInput = function(timeline){
	var start = new Date(timeline.start);
	var end = new Date(timeline.end);
	// Hours are returned in 24 hour time.
	var startHours = start.getHours();
	var startAMPM = startHours >= 12 ? 'PM' : 'AM';
	if(startHours > 12) {
		startHours -= 12;
	} else if(startHours == 0) {
		startHours = 12;
	}
	var endHours = end.getHours();
	var endAMPM = startHours >= 12 ? 'PM' : 'AM';
	if(endHours > 12) {
		endHours -= 12;
	} else if(endHours == 0) {
		endHours = 12;
	}
	// Need to convert the minutes to strings with a padded 0
	var startMinutes = start.getMinutes();
	if(startMinutes < 10){
		startMinutes = '0' + startMinutes;
	}
	var endMinutes = end.getMinutes();
	if(endMinutes < 10){
		endMinutes = '0' + endMinutes;
	}
	return {
		startHours: startHours,
		startMinutes: startMinutes,
		startAMPM: startAMPM,
		endHours: endHours,
		endMinutes: endMinutes,
		endAMPM: endAMPM
	};
};

var onAddHashtag = function(){
	var hastagInput = $('#hashtagInput');
	// Get the entry from the text input
	var inputText = hastagInput.val().trim();
	// Clear the entry
	hastagInput.val('');
	// Split the entry and remove any hashtags that might have been added by the user.
	var entries = inputText.replace(/(#)/g, '').split(',').map(function(entry){return entry.trim()});
	for(var i = 0; i < entries.length; i++) {
		addHashtag(entries[i]);
	}
};

var addHashtag = function(hashtag) {
	var displayGroup = $('#hashtagsDisplay');
	// Add a new bubble to the display group
	if(hashtag != '') {
		hashtag = '#'+hashtag;
		var count = hashtags.length;
		hashtags.push(hashtag);
		// Give each bubble an index so we can delete them later.
		var bubble = '<span class="hashtag label label-default" id="hashtag_' + count + '">' + hashtag + '<span class="btn btn-xs btn-remove glyphicon glyphicon-remove"></span></span>';
		displayGroup.append(bubble);
		$('#hashtagsDisplay #hashtag_' + count).data('hashtag', hashtag);
	}
}

$(function () {
	$('#startdatetimepicker').datetimepicker();
	$('#enddatetimepicker').datetimepicker();
	$(document)
		.keypress(function(e) {
		    if(e.which == 13) {
		    	e.preventDefault();
		    }
		})
		.on('click', '.btn-add', function(e) {
			e.preventDefault();
			onAddHashtag();
		})
		.on('click', '.btn-remove', function(e) {
			e.preventDefault();
			var elemData = $(this).parent().data();
			$(this).parent().remove();
			hashtags.splice(hashtags.indexOf(elemData.hashtag), 1);
			return false;
		});

	$(document).ready(function() {
		$('#hashtagInput').keypress(function(e) {
		    if(e.which == 13) {
		    	e.preventDefault();
		        onAddHashtag();
		    }
		});
	});	
});
