var TimelineStatus = {
	PENDING:0,
	READY:1,
	RUNNING:2,
	PROCESSING:3,
	COMPLETE:4,
	REMOVED:5,
	ERROR:6
};

var TimelineStatusToString = function(timelineStatus){
	switch(+timelineStatus) {
		case TimelineStatus.PENDING:
			return 'Pending';
		case TimelineStatus.READY:
			return 'Ready';
		case TimelineStatus.RUNNING:
			return 'Running...';
		case TimelineStatus.PROCESSING:
			return 'Processing Tweets';
		case TimelineStatus.COMPLETE:
			return 'Complete!';
		case TimelineStatus.ERROR:
			return 'Errored :(';
	}
	return '';
}

Timeline = function(raw_timeline){
	this._id = raw_timeline ? raw_timeline._id : null;
	this.name = raw_timeline ? raw_timeline.name : '';
	this.start = raw_timeline ? raw_timeline.start : null;
	this.end = raw_timeline ? raw_timeline.end : null;
	this.hashtags = raw_timeline ? raw_timeline.hashtags.concat() : [];
	this.status = raw_timeline ? raw_timeline.status : TimelineStatus.READY;
	this.plotpoints = raw_timeline ? raw_timeline.plotpoints.concat() : []; //of objects {tstamp: Date, text: 'Description of plot point'}
	this.plotpoints.forEach(function(plotpoint){
		// Convert to date
		if(plotpoint.timestamp.constructor === String){
			plotpoint.timestamp = new Date(plotpoint.timestamp);
		}
	});
};

Timeline.prototype.constructor = Timeline;