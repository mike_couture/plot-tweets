$(function () {
    $('#startdatetimepicker').datetimepicker();
    $('#enddatetimepicker').datetimepicker();
    $(document)
        .keypress(function(e) {
            if(e.which == 13) {
                e.preventDefault();
                return false;
            }
        })
        .on('click', '.btn-add', function(e) {
            e.preventDefault();
            onAddHashtag();
        })
        .on('click', '.btn-remove', function(e) {
            e.preventDefault();
            var elemData = $(this).parent().data();
            $(this).parent().remove();
            hashtags.splice(hashtags.indexOf(elemData.hashtag), 1);
            return false;
        });

    $('#submitTimeline').validator().on('submit', function (e) {
        event.preventDefault();
        if (e.isDefaultPrevented()) {
        } else {
            // validate dates
            var invalidReason = validateDates();
            if(invalidReason){
                d3.select('#errorLabel').classed('hidden', false).text(invalidReason);
            } else  {
                d3.select('#errorLabel').classed('hidden', true).text(invalidReason);
                addTimeline();
            }
        }
    });

    var hashtags = [];

    var getStartDate = function(){
        var d = $('#startdatetimepicker').data('DateTimePicker').date();
        if(d == null) {
            return null;
        }
        return d.toDate();
    };

    var getEndDate = function(){
        var d = $('#enddatetimepicker').data('DateTimePicker').date();
        if(d == null) {
            return null;
        }
        return d.toDate();
    };

    function validateDates(){
        var start = getStartDate();
        var end = getEndDate();
        if(start == null){
            return 'Enter a start date.';
        }
        if(end == null){
            return 'Enter an end date.';
        }
        var now = new Date();
        var startMS = start.getTime();
        var endMS = end.getTime();
        if(endMS < now.getTime()){
            return 'End date cannot be in the past.';
        }
        if(endMS < startMS){
            return 'End date cannot come before the start date.'
        }
        if(startMS == endMS){
            return 'Need more time in between start and end!'
        }
        if(endMS - startMS > (1000 * 60 * 60 * 6)){
            return 'Timelines are limited to 6 hours.'
        }
        return null;
    };

    var onAddHashtag = function(){
        var hastagInput = $('#hashtagInput');
        // Get the entry from the text input
        var inputText = hastagInput.val().trim();
        // Clear the entry
        hastagInput.val('');
        // Split the entry and remove any hashtags that might have been added by the user.
        var entries = inputText.replace(/(#)/g, '').split(',').map(function(entry){return entry.trim()});
        for(var i = 0; i < entries.length; i++) {
            addHashtag(entries[i]);
        }
    };

    var addHashtag = function(hashtag) {
        var displayGroup = $('#hashtagsDisplay');
        // Add a new bubble to the display group
        if(hashtag != '') {
            hashtag = '#'+hashtag;
            var count = hashtags.length;
            hashtags.push(hashtag);
            // Give each bubble an index so we can delete them later.
            var bubble = '<span class="hashtag label label-default" id="hashtag_' + count + '">' + hashtag + '<span class="btn btn-xs btn-remove glyphicon glyphicon-remove"></span></span>';
            displayGroup.append(bubble);
            $('#hashtagsDisplay #hashtag_' + count).data('hashtag', hashtag);
        }
    }

	var addTimeline = function() {
        var start = $('#startdatetimepicker').data('DateTimePicker').date().toDate();
        var end = $('#enddatetimepicker').data('DateTimePicker').date().toDate();
        var hastagInput = $('#hashtagInput');
        // Get the entry from the text input
        var inputText = hastagInput.val().trim();
        // Split the entry and remove any hashtags that might have been added by the user.
        hashtags = inputText.replace(/(#)/g, '').split(',').map(function(entry){return '#' + entry.trim()});
        // Add our own hashtags
        var timeline = new Timeline();
        timeline.name = $('#timelineName').val();
        timeline.start = start;
        timeline.end = end;
        timeline.hashtags = hashtags;
        // TODO: Do not make this ready right away
        timeline.status = TimelineStatus.READY;

        // Post to the DB
        $.ajax({
            type: 'POST',
            data: JSON.stringify(timeline),
            url: '/timeline',
            contentType: 'application/json'
        }).done(function( response ) {
            // Check for successful (blank) response
            if (response.msg === '') {
                window.location.replace('archive');
            }
            else {
                console.log('Error: ' + response.msg);
            }
        });
	};
});
