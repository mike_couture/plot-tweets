$(function () {
	$(document).ready(function() {
	    populateList();
	});

	var dateFormat = 'M/D/YYYY';
	var timeFormat = 'h:mm A';

	var populateList = function(){
	    var listContent = '';

	    // jQuery AJAX call for JSON
	    $.getJSON( '/timeline', function( data ) {
	    	d3.select('#timelines tbody').selectAll("*").remove();

	    	// Use fancy d3 selectors
	    	var rows = d3.select('#timelines tbody')
	    		.selectAll('tr')
	    		.data(data).enter()
	    		.append('tr');

	    	var actions = rows.append('td');
	    	actions.append('div')
	    		.html(function(d){
		    		if(+d.status == TimelineStatus.COMPLETE){
		    			return '<a href="/view?timelineID=' + d._id +'">View Timeline</a>';
		    		} else {
		    			return TimelineStatusToString(+d.status);
		    		}
		    	})
	    		.attr('class', function(d){
					switch(+d.status) {
	    				case TimelineStatus.READY:
	    				case TimelineStatus.RUNNING:
	    					return 'text-warning';
	    				case TimelineStatus.COMPLETE:
	    					return 'text-success';
	    				case TimelineStatus.ERROR:
	    					return 'text-danger';
	    			}
	    			return null;
	    		});
	    	actions.append('div')
	    		.html(function(d){
	    			return '<a href="/plotpoints?timelineID=' + d._id +'">Add Plot Points</a>'
		    	});

	    	rows.append('td')
	    		.text(function(d){
	    			return d.name;
	    		});

	    	var startTime = rows.append('td');
	    	startTime.append('div')
	    		.text(function(d){
	    			return moment(d.display_start).format(dateFormat);
	    		})
	    		.style('font-size', '11px');
	    	startTime.append('div')
	    		.text(function(d){
	    			return moment(d.display_start).format(timeFormat);
	    		});

	    	var endTime = rows.append('td');
	    	endTime.append('div')
	    		.text(function(d){
	    			return moment(d.display_end).format(dateFormat);
	    		})
	    		.style('font-size', '11px');
	    	endTime.append('div')
	    		.text(function(d){
	    			return moment(d.display_end).format(timeFormat);
	    		});

	    	rows.append('td')
	    		.text(function(d){
            		var hashtags = d.hashtags;
            		if(hashtags.constructor === Array)
	    				return hashtags.join(', ');
	    			return hashtags;
	    		});

		    rows.selectAll('.btn-process').on('click', function(d){
		    	var node = this;
		    	$.ajax({
		            type: 'GET',
		            url: '/archive/process/' + d._id
		        }).done(function( response ) {
		            // Check for a successful (blank) response
		            if (response.msg === '') {
		            	var test = d3.select(node);
		            }
		            else {
		                alert('Error: ' + response.msg);
		            }
		            // Update the table
		            populateList();
		        });
		    });
	    });
	};
});