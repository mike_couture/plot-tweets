angular.module('archive', [])

.controller('ArchiveCtrl', ['$scope', 'Timeline',
	function ($scope, Timeline) {
		// Get timelines and parse them to insert into the table
		Timeline.query(function (res){
			angular.forEach(res, function (timeline){
				if(+timeline.status == TimelineStatus.COMPLETE){
					timeline.status_link = '<a href="/view?timelineID=' + timeline._id +'">View Timeline</a>';
				} else {
					timeline.status_link = '<span>' + TimelineStatusToString(+timeline.status) + '</span>';
				}
			});
			$scope.timelines = res;
		});
	}
]);