angular.module('plotTweetsApp', [
	'ngResource',
    'archive',
    'plotPoints'
])

.filter('toList', function() {
	return function(list) {
		return list.join(', ');
	};
})

.filter('toTrusted', ['$sce', function($sce){
    return function(text) {
        return $sce.trustAsHtml(text);
    };
}])

.filter('status', function() {
	return function(timeline) {
		return TimelineStatusToString(timeline.status);
	};
})

.factory('Timeline', ['$resource', function ($resource){
	return $resource('timeline/:timelineID', null,
		{
			query: {method:'GET', isArray:true},
			update: {method:'PUT'}
		});
	}
]);