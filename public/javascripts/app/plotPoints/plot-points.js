angular.module('plotPoints', ['ngResource'])

.directive('datetimepicker', [function (){
	return {
		restrict: 'A',
		scope: false,
		link: function (scope, elem, attrs){
		}
	}
}])

.directive('plotpointModal', [function (){
	return {
		restrict: 'A',
		scope: false,
		link: function (scope, elem, attrs){
            $(elem).on('show.bs.modal', function(e) {
            	$('#description').val('');
            });
            $(elem).on('hide.bs.modal', function(e) {
            	scope.$apply(function (){
            		scope.newPlotpoint = null;
            		scope.description = '';
            	});
            });
		}
	}
}])

.directive('createAutoPlotpoint', [function (){
	return {
		restrict: 'A',
		scope: false,
		link: function (scope, elem, attrs){
            $(elem).click(function() {
            	scope.$apply(function (){
            		var tstamp = new Date(scope.tstamp);
            		$('#timepicker').datetimepicker({
            			dayViewHeaderFormat: 'MMM YYYY h:mm a',
						defaultDate: tstamp}
					);
            		scope.newPlotpoint = {timestamp: tstamp, description:'', is_public: true, edit_tstamp: false};
            	});
                $('#plotPointModal').modal({
	        		backdrop: 'static'
	    		});
            });
		}
	}
}])

.controller('PlotPointsCtrl', ['$scope', 'Timeline', '$document', '$interval', '$timeout',
	function ($scope, Timeline, $document, $interval, $timeout) {
		var refreshPromise, clockPromise;

		var timelineID = getParameterByName('timelineID');

		$scope.timeline = {name: '...'};
		$scope.pending = [];
		$scope.tstamp = Date.now();
		$scope.newPlotPoint = null;
		$scope.contentLoaded = false;
		$scope.description = '';
		$scope.refreshing = false;

		// Set up the timer for the time display
		var tick = function() {
			$scope.tstamp = Date.now();
		}
		tick();
		var clockPromise = $interval(tick, 1000);

		// Query every second. TODO: Use Pusher or something else for this.
		var refresh = function() {
			$timeout.cancel(refreshPromise);
			console.log('running');
			Timeline.query({timelineID: timelineID}, function (res){
				var timeline = res[0];
				timeline.plotpoints.sort(function (a, b){
					var da = new Date(a.timestamp).getTime();
	                var db = new Date(b.timestamp).getTime();
	                if (da > db) {
	                    return -1;
	                }
	                if (da < db) {
	                    return 1;
	                }
	                return 0;
				});
				$scope.timeline = timeline;
				$scope.contentLoaded = true;
				$scope.refreshing = false;
				// If the timeline is still running, keep refreshing
				if(+timeline.status == TimelineStatus.RUNNING){
					refreshPromise = $timeout(refresh, 1000);
				}
			});
		}

		$document.ready(function () {
			refresh();
	    });

		$scope.stop = function (){
			$interval.cancel(clockPromise);
			$timeout.cancel(refreshPromise);
		};

		$scope.submitPlotpoint = function (pp){
			// scope holds the modified plotpoint
			$scope.refreshing = true;
			if($scope.newPlotpoint.edit_tstamp) {
				$scope.newPlotpoint.timestamp = $('#timepicker').data('DateTimePicker').date().toDate();
			}
			$scope.newPlotpoint.description = $('#description').val();
			delete $scope.newPlotpoint.edit_tstamp;
			$scope.timeline.plotpoints.unshift($scope.newPlotpoint);
			Timeline.update({timelineID: timelineID}, $scope.timeline);
		};

		$scope.onEditTstamp = function (){
			$scope.newPlotpoint.edit_tstamp = true;
		};

	    $scope.$on('$destroy', function() {
	    	console.log('destroy');
			$scope.stop();
	    });
	}
]);
