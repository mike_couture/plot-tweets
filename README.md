# Project Setup

Once the project has been cloned, please do the following:

`cd` into the project's directory.

`npm install`

`bower install`

`mkdir data` for mongo.

Install [mongodb](https://www.mongodb.org/downloads#production)

After mongo installation, you may need to add the bin directory to your environment variables (usualy located at C:\Program Files\MongoDB\Server\3.2\bin).

###After everything is installed, set up the DB###

Open a new terminal and run `mongod --dbpath [path to data directory]`. Leave the terminal up, this is the DB instance.

Start the server with `npm start`.

Visit [http://localhost:3000](http://localhost:3000).
