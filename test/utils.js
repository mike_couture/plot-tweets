var mongoose = require('mongoose');
var request = require('supertest');

var conf = require('../confs/test');

before(function (done){
	// Close main connection and reopen with test configs
	mongoose.connection.close(function (){
	    mongoose.connect(conf.db_connection);							
    	done();
	});
});
