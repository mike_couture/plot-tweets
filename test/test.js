var assert = require('chai').assert;
var should = require("should");

var mongoose = require('mongoose');
var request = require('supertest');

var conf = require('../confs/test');

var app = require("../app");

var utils = require('./utils.js');

var testTimeline = {
    name: 'Test Timeline Router Post',
    start: new Date(2016, 4, 1, 17, 30),
    end: new Date(2016, 4, 1, 19, 30),
    display_start: new Date(2016, 4, 1, 18),
    display_end: new Date(2016, 4, 1, 19),
    hashtags: ['#test', '#mongoose', '#models'],
    plotpoints: [{timestamp: '2016-04-01T22:10:00.000Z', description: 'Test point 1', is_public: false},
                {timestamp: '2016-04-01T22:35:00.000Z', description: 'Test point 2', is_public: false}],
    status: 2
};

describe('Routing', function (){
  	var url = 'http://localhost:8080';
  	var newTimeline = null;

	before(function (done){
		// Close main connection and reopen with test configs
		mongoose.connection.close(function (){
		    mongoose.connect(conf.db_connection);							
	    	done();
		});
	});

	describe('POST /', function(){
	  	it('should add a new timeline', function(done){
	  		request(url).post('/')
	  			.send(testTimeline)
				.end(function(err, res) {
					should.not.exist(err);
			        res.status.should.equal(200);
			        newTimeline = res;
			        done();
			    });
	  		});
	  	});
	});

	describe('GET /timeline', function(){
	  	it('should return all 3 test timelines', function(done){
	  		request(url).get('/timeline')
			    // end handles the response
				.end(function(err, res) {
					should.not.exist(err);
			        res.status.should.equal(200);
			        done();
			    });
	  		});
	  	});
	});

	after(function (done){
		// Close main connection and reopen with test configs
		mongoose.connection.db.dropDatabase(function (){
			done();
		});
	});
});
