'use strict';
var should = require('should');

var Coordinate = require('../../app/models/coordinate');

describe('Coordinate: model', function () {
    describe('#create()', function (){
    	var coords = [-77.06079574, 38.80650431];
        it('should create a new Coordinate model', function (done){
            Coordinate.create({'timeline_id': '1', coordinates: coords}, function (err, res){
            	should.not.exist(err);
            	res.timeline_id.should.equal('1');
            	done();
            });
        });
    });
});
