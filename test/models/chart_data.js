'use strict';
var should = require('should');

var ChartData = require('../../app/models/chart_data');

describe('ChartData: model', function () {
    describe('#create()', function (){
    	var cd = {
			timeline_id: '1',
			count: 50,
			words: ['one', 'two', 'three'],
			timestamp: new Date(2016, 4, 1, 17)
		};
        it('should create a new ChartData model', function (done){
            ChartData.create(cd, function (err, res){
            	should.not.exist(err);
            	res.timeline_id.should.equal('1');
            	res.count.should.equal(50);
            	new Date(res.timestamp).getTime().should.equal(new Date(2016, 4, 1, 17).getTime());
            	done();
            });
        });
    });
});
