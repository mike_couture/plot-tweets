'use strict';
var should = require('should');

var Timeline = require('../../app/models/timeline');

var testTimeline = {
    name: 'Test',
    start: new Date(2016, 4, 1, 17, 30),
    end: new Date(2016, 4, 1, 19, 30),
    display_start: new Date(2016, 4, 1, 18),
    display_end: new Date(2016, 4, 1, 19),
    hashtags: ['#test', '#mongoose', '#models'],
    plotpoints: [{timestamp: '2016-04-01T22:10:00.000Z', description: 'Test point 1', is_public: false},
                {timestamp: '2016-04-01T22:35:00.000Z', description: 'Test point 2', is_public: false}],
    status: 2
};

describe('Timeline: model', function () {
    describe('new Timeline()', function () {
        it('should create a new Timeline', function (done) {
            Timeline.create(testTimeline, function (err, createdTimeline) {
                // Confirm that that an error does not exist
                should.not.exist(err);
                // verify that the returned timeline is what we expect
                createdTimeline.name.should.equal('Test');
                // Call done to tell mocha that we are done with this test
                done();
            });
        });
    });

    describe('#validateDates()', function () {
        it('should not allow invalid start and end dates', function (done){
            Timeline.validateDates(new Date(''), new Date('')).should.equal(false);
            done();
        });
        it('should not allow end dates in the past', function (done){
            Timeline.validateDates(new Date(), new Date(2014)).should.equal(false);
            done();
        });
        it('should not allow end dates before start dates', function (done){
            var dates = validDates();
            dates.start.setHours(dates.start.getHours() + 6);
            dates.end.setHours(dates.end.getHours() + 2);
            Timeline.validateDates(dates.start, dates.end).should.equal(false);
            done();
        });
        it('should not allow equal dates', function (done){
            var dates = validDates();
            Timeline.validateDates(dates.start, dates.start).should.equal(false);
            done();
        });
        it('should not allow dates greater than 6 hours', function (done){
            var dates = validDates();
            dates.start.setHours(dates.start.getHours() - 4);
            dates.end.setHours(dates.end.getHours() + 4);
            Timeline.validateDates(dates.start, dates.end).should.equal(false);
            done();
        });
        it('should allow valid dates', function (done){
            var dates = validDates();
            Timeline.validateDates(dates.start, dates.end).should.equal(true);
            done();
        });
    });

    describe('#createFromObject()', function () {
        var dates = validDates();
        var testTimeline = {
            name: 'Test from raw object',
            start: dates.start.toString(),
            end: dates.end.toString(),
            hashtags: ['#test', '#mongoose', '#models'],
            plotpoints: [],
            status: 2
        };
        var newTimeline = null;
        it('should create a new Timeline from a raw object', function (done) {
            newTimeline = Timeline.createFromObject(testTimeline);
            newTimeline.name.should.equal('Test from raw object');
            done();
        });
        // When created, the raw start and end become the display start and end.
        it('should have display times equal to the original times', function (done){
            var start = new Date(testTimeline.start);
            var end = new Date(testTimeline.end);
            newTimeline.display_start.getTime().should.equal(start.getTime());
            newTimeline.display_end.getTime().should.equal(end.getTime());
            done();
        });
        // The start and end date should be 30 minutes before and after
        it('should have start and end times equal to the original times offset by 30 minutes', function (done){
            var expectedStart = new Date(testTimeline.start);
            expectedStart.setMinutes(expectedStart.getMinutes() - 30);
            var expectedEnd = new Date(testTimeline.end);
            expectedEnd.setMinutes(expectedEnd.getMinutes() + 30);

            newTimeline.start.getTime().should.equal(expectedStart.getTime());
            newTimeline.end.getTime().should.equal(expectedEnd.getTime());
            done();
        });
    });

});

// The Timeline methods require valid dates to pass
function validDates(){
    var startDate = new Date();
    startDate.setHours(startDate.getHours() - 1, 0, 0, 0);
    var endDate = new Date();
    endDate.setHours(endDate.getHours() + 2, 0, 0, 0);
    return {start: startDate, end: endDate};
};