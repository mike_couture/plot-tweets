'use strict';
var should = require('should');

var Tweet = require('../../app/models/tweet');

describe('Tweet: model', function () {
    describe('#createFromObject()', function (){
        it('should create a Tweet from raw JSON', function (done){
            var tweet = Tweet.createFromObject(rawTweet);
            should.exist(rawTweet);
            tweet.id_str.should.equal('723612599913459712');
            done();
        });
    });
});

var rawTweet = { created_at: 'Fri Apr 22 20:41:02 +0000 2016',
  id: 723612599913459700,
  id_str: '723612599913459712',
  text: 'Testing multiple #plottweettest #testingplottweets',
  source: '<a href="http://twitter.com/download/android" rel="nofollow">Twitter for Android</a>',
  truncated: false,
  in_reply_to_status_id: null,
  in_reply_to_status_id_str: null,
  in_reply_to_user_id: null,
  in_reply_to_user_id_str: null,
  in_reply_to_screen_name: null,
  user:
   { id: 723606828341444600,
     id_str: '723606828341444608',
     name: 'Poot Sanchez',
     screen_name: 'PootSanchez1',
     location: null,
     url: null,
     description: null,
     protected: false,
     verified: false,
     followers_count: 0,
     friends_count: 0,
     listed_count: 0,
     favourites_count: 0,
     statuses_count: 5,
     created_at: 'Fri Apr 22 20:18:06 +0000 2016',
     utc_offset: null,
     time_zone: null,
     geo_enabled: false,
     lang: 'en',
     contributors_enabled: false,
     is_translator: false,
     profile_background_color: 'F5F8FA',
     profile_background_image_url: '',
     profile_background_image_url_https: '',
     profile_background_tile: false,
     profile_link_color: '2B7BB9',
     profile_sidebar_border_color: 'C0DEED',
     profile_sidebar_fill_color: 'DDEEF6',
     profile_text_color: '333333',
     profile_use_background_image: true,
     profile_image_url: 'http://abs.twimg.com/sticky/default_profile_images/default_profile_1_normal.png',
     profile_image_url_https: 'https://abs.twimg.com/sticky/default_profile_images/default_profile_1_normal.png',
     default_profile: true,
     default_profile_image: true,
     following: null,
     follow_request_sent: null,
     notifications: null },
  geo: null,
  coordinates: {type: 'Point', coordinates: [-77.06079574, 38.80650431]},
  place: null,
  contributors: null,
  is_quote_status: false,
  retweet_count: 0,
  favorite_count: 0,
  entities:
   { hashtags: [ { text: 'plottweettest', indices:[] }, { text: 'testingplottweets', indices:[] } ],
     urls: [],
     user_mentions: [],
     symbols: [] },
  favorited: false,
  retweeted: false,
  filter_level: 'low',
  lang: 'en',
  timestamp_ms: '1461357662548' }