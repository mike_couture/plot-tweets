#!/bin/bash
echo 'Stopping forever...'
/home/plot_tweets/plot-tweets/node_modules/forever/bin/forever stop app.js
echo 'Updating...'
git pull
echo 'Setting to production env...'
export NODE_ENV=production
echo 'Starting forever...'
/home/plot_tweets/plot-tweets/node_modules/forever/bin/forever start app.js